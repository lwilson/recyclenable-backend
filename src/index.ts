import express, {Application, Response, Request} from 'express';
import {Query, Item, getItems} from './db';

const port: number = 8000;

const app: Application = express();

app.use(express.json());

app.get('/api/version', (req: Request, res: Response) => {
  console.log('GET /api/version');
  res.send('1.0.0');
});

app.post('/api/getitems', async (req: Request, res: Response) => {
  console.log('GET /api/getitems');
  res.send(await getItems(req.body));
});

app.listen(port, () => {console.log('server listening on port: ' + port);});
