import * as dotenv from 'dotenv';
dotenv.config();

import postgres from 'postgres';
const sql = postgres()

export interface Query {
  loc: string;
  name?: string;
  dirty?: boolean;
  logo?: string;
  material?: string;
}

export interface Item {
  recyclable: boolean;
  props: Query;
  msg?: string;
  products?: string;
  imgURL?: string;
  imgCredit?: string;
}

const resultToItems = (r: any[]): Item[] => {
  let res: Item[] = [];
  r.forEach(item => {
    res.push({
      recyclable: item.recyclable,
      msg: item.msg,
      products: item.products,
      imgURL: item.imgurl,
      imgCredit: item.imgcredit,
      props: {
        loc: item.loc,
        name: item.name,
        dirty: item.dirty,
        logo: item.logo,
        material: item.material
      }
    });
  });
  return res;
}

const columns = ['recyclable', 'msg', 'products', 'imgurl', 'imgcredit', 'loc',
  'name', 'dirty', 'logo', 'material'];

export const getItems = async (q: Query): Promise<Item[]> => {
  return resultToItems(await sql`select ${sql(columns)} from items where
    loc = ${q.loc} and
    ${q.name == null ? sql`` : sql`(name is NULL or name = ${q.name}) and`}
    ${q.dirty == null ? sql`` : sql`(dirty is NULL or dirty = ${q.dirty}) and`}
    ${q.logo == null ? sql`` : sql`(logo is NULL or logo = ${q.logo}) and`}
    ${q.material == null ? sql``
      : sql`(material = ${q.material}) and`}
    1=1`);
}
